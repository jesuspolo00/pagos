<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';

$descuento = str_replace(',', '', $_POST['descuento']);
$iva = $_POST['iva'];
$mensualidad = str_replace(',', '', $_POST['mensualidad']);

$subtotal = $mensualidad - $descuento;
if ($iva == 0) {
    $total = $subtotal;
} else {
    $total = ($subtotal * $iva) / 100;
}

echo json_encode(['total_final' => $total, 'descuento' => $descuento, 'mensualidad' => $mensualidad]);
