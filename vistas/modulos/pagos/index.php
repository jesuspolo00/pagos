<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre'] && $_SESSION['rol'] != 5) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'pagos' . DS . 'ControlPagos.php';

$instancia = ControlPagos::singleton_pagos();

$datos_cursos = $instancia->mostrarCursosControl();
$datos_estudiantes = $instancia->mostrarEstudiantesControl();

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3">
                    <h4 class="m-0 font-weight-bold text-danger">Pagos</h4>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <div class="row">
                            <div class="col-lg-6">
                                <select name="salon" class="form-control filtro_change">
                                    <option value="" selected>Seleccione una opcion...</option>
                                    <?php
                                    foreach ($datos_cursos as $curso) {
                                        $id_curso = $curso['id'];
                                        $nombre = $curso['nombre'];
                                        $estado = $curso['activo'];

                                        $ver = ($estado == 2) ? '' : 'd-none';

                                    ?> <option value="<?= $nombre ?>" class="<?= $ver ?>"><?= $nombre ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-6 form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control col-lg-12 filtro" placeholder="Buscar...">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover table-borderless table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">#</th>
                                    <th scope="col">Documento</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Genero</th>
                                    <th scope="col">Curso</th>
                                </tr>
                            </thead>
                            <tbody class="buscar">
                                <?php
                                foreach ($datos_estudiantes as $estudiante) {
                                    $id_estudiante = $estudiante['id'];
                                    $documento = $estudiante['documento'];
                                    $nombre_completo = $estudiante['nombre'] . ' ' . $estudiante['apellido'];
                                    $genero = ($estudiante['genero'] == 1) ? 'Masculino' : 'Femenino';
                                    $curso = $estudiante['curso'];
                                    $ver = ($curso == '' || $curso == 'Egresados') ? 'd-none' : '';

                                    $anio_mes_actual = date('Y-n');
                                    $ultimo_pago = $instancia->ultimoPagoEstudianteControl($id_estudiante, $anio_mes_actual);

                                    $anio_mes = $ultimo_pago['anio_mes'];

                                    if ($anio_mes != '') {
                                        $pagar = 'd-none';
                                        $descargar = '';
                                    } else {
                                        $pagar = '';
                                        $descargar = 'd-none';
                                    }
                                ?>
                                    <tr class="text-center <?= $ver ?>">
                                        <td><?= $id_estudiante ?></td>
                                        <td><?= $documento ?></td>
                                        <td><a href="<?= BASE_URL ?>pagos/historial?estudiante=<?= base64_encode($id_estudiante) ?>"><?= $nombre_completo ?></a></td>
                                        <td><?= $curso ?></td>
                                        <td><?= $genero ?></td>
                                        <td class="<?= $pagar ?>">
                                            <a href="<?= BASE_URL ?>pagos/pagar?estudiante=<?= base64_encode($id_estudiante) ?>" class="btn btn-success btn-sm" id="<?= $id_estudiante ?>" data-tooltip="tooltip" title="Pagar mes" data-placement="bottom">
                                                <i class="fas fa-money-check-alt"></i>
                                            </a>
                                        </td>
                                        <td class="<?= $descargar ?>">
                                            <a href="<?= BASE_URL ?>imprimir/factura?pago=<?= base64_encode($ultimo_pago['id']) ?>" target="_blank" class="btn btn-primary btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Descargar factura">
                                                <i class="fa fa-download"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
