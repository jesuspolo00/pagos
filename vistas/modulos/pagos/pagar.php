<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre'] && $_SESSION['rol'] != 5) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'pagos' . DS . 'ControlPagos.php';

$instancia = ControlPagos::singleton_pagos();

if (isset($_GET['estudiante'])) {
    $id_estudiante = base64_decode($_GET['estudiante']);

    $datos_estudiante = $instancia->mostrarDatosEstudiantesControl($id_estudiante);
    $genero = ($datos_estudiante['genero'] == 1) ? 'Masculino' : 'Femenino';

    $datos_empresa_factura = $instancia->mostrarEmpresaFacturaControl();
    $datos_meses = $instancia->mostrarMesesControl();
    $datos_anios = $instancia->mostrarAniosControl();

    $anio_actual = date('Y');

?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-sm mb-4">
                    <div class="card-header py-3">
                        <h4 class="m-0 font-weight-bold text-danger">
                            <a href="<?= BASE_URL ?>pagos/index" class="text-decoration-none">
                                <i class="fa fa-arrow-left text-danger"></i>
                            </a>
                            &nbsp;
                            Pagar mensualidad
                        </h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" id="pagar">
                            <input type="hidden" name="id_log" value="<?= $id_log ?>">
                            <input type="hidden" name="id_estudiante" value="<?= $id_estudiante ?>">
                            <div class="row p-2">
                                <div class="col-lg-12 mb-2">
                                    <h4 class="text-danger">Informacion del estudiante</h4>
                                </div>
                                <div class="col-lg-4 form-group">
                                    <label>Documento</label>
                                    <input type="text" class="form-control numeros" disabled value="<?= $datos_estudiante['documento'] ?>">
                                </div>
                                <div class="col-lg-4 form-group">
                                    <label>Nombre estudiante</label>
                                    <input type="text" class="form-control numeros" disabled value="<?= $datos_estudiante['nombre'] . ' ' . $datos_estudiante['apellido'] ?>">
                                </div>
                                <div class="col-lg-4 form-group">
                                    <label>Genero</label>
                                    <input type="text" class="form-control numeros" disabled value="<?= $genero ?>">
                                </div>
                                <div class="col-lg-4 form-group">
                                    <label>Curso actual</label>
                                    <input type="text" class="form-control numeros" disabled value="<?= $datos_estudiante['curso'] ?>">
                                </div>
                                <div class="col-lg-12 mt-2 mb-2">
                                    <h4 class="text-danger">Informacion de pago</h4>
                                </div>
                                <?php
                                foreach ($datos_empresa_factura as $empresa) {
                                    $id_empresa = $empresa['id'];
                                    $nombre_empresa = $empresa['nombre'];
                                    $iva = $empresa['iva'];
                                    $mensualidad = $empresa['mensualidad'];
                                ?>
                                    <input type="hidden" value="<?= $iva ?>" id="iva<?= $id_empresa ?>">
                                    <input type="hidden" value="<?= $mensualidad ?>" id="men<?= $id_empresa ?>">
                                    <div class="col-lg-4 form-group mt-3">
                                        <div class="custom-control custom-radio ml-2">
                                            <input type="radio" class="custom-control-input emp_fac" required id="<?= $id_empresa ?>" value="<?= $id_empresa ?>" name="empresa_factura">
                                            <label class="custom-control-label" for="<?= $id_empresa ?>"><?= $nombre_empresa ?></label>
                                        </div>
                                    </div>
                                <?php
                                }
                                ?>
                                <div class="col-lg-12 mb-4"></div>
                                <div class="col-lg-4 form-group mt-2">
                                    <label>Mes a pagar</label>
                                    <select name="mes" class="form-control" required>
                                        <option value="" selected>Seleccione una opcion</option>
                                        <?php
                                        foreach ($datos_meses as $mes) {
                                            $id_mes = $mes['id'];
                                            $nombre = $mes['nombre'];
                                        ?>
                                            <option value="<?= $id_mes ?>"><?= $nombre ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-4 form-group mt-2">
                                    <label>A&ntilde;o a pagar</label>
                                    <select name="anio" class="form-control" required>
                                        <?php
                                        foreach ($datos_anios as $anio) {
                                            $nombre_anio = $anio['nombre'];
                                            $select = ($anio_actual == $nombre_anio) ? 'selected' : '';
                                        ?>
                                            <option value="<?= $nombre_anio ?>" <?= $select ?>><?= $nombre_anio ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-4 form-group mt-2">
                                    <label>IVA</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">%</span>
                                        </div>
                                        <input type="text" class="form-control numeros" id="iva" name="iva" value="0" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-4 form-group">
                                    <label>Mensualidad</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">$</span>
                                        </div>
                                        <input type="text" class="form-control numeros mensualidad" name="mensualidad" required value="0">
                                    </div>
                                </div>
                                <div class="col-lg-4 form-group">
                                    <label>Descuento</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">$</span>
                                        </div>
                                        <input type="text" class="form-control numeros descuento" name="descuento" value="0">
                                    </div>
                                </div>
                                <div class="col-lg-4 form-group">
                                    <label>Total a pagar</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">$</span>
                                        </div>
                                        <input type="text" class="form-control numeros total" disabled value="0">
                                    </div>
                                </div>
                                <div class="col-lg-12 mt-3">
                                    <button type="submit" class="btn btn-success btn-sm float-right">
                                        <i class="fa fa-save"></i>
                                        &nbsp;
                                        Guardar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php
    include_once VISTA_PATH . 'script_and_final.php';

    if (isset($_POST['mensualidad'])) {
        $instancia->guardarPagoControl();
    }
}
    ?>
    <script src="<?= PUBLIC_PATH ?>js/pagos/funcionesPagos.js"></script>