<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre'] && $_SESSION['rol'] != 5) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'pagos' . DS . 'ControlPagos.php';

$instancia = ControlPagos::singleton_pagos();

if (isset($_GET['estudiante'])) {

    $id_estudiante = base64_decode($_GET['estudiante']);

    $datos_estudiante = $instancia->mostrarDatosEstudiantesControl($id_estudiante);
    $genero = ($datos_estudiante['genero'] == 1) ? 'Masculino' : 'Femenino';

    $datos_pago = $instancia->mostrarDatosPagoEstudianteControl($id_estudiante);
?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-sm mb-4">
                    <div class="card-header py-3">
                        <h4 class="m-0 font-weight-bold text-danger">
                            <a href="<?= BASE_URL ?>pagos/index" class="text-decoration-none">
                                <i class="fa fa-arrow-left text-danger"></i>
                            </a>
                            &nbsp;
                            Historial de pagos
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="row p-2">
                            <div class="col-lg-12 mb-2">
                                <h4 class="text-danger">Informacion del estudiante</h4>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label>Documento</label>
                                <input type="text" class="form-control numeros" disabled value="<?= $datos_estudiante['documento'] ?>">
                            </div>
                            <div class="col-lg-4 form-group">
                                <label>Nombre estudiante</label>
                                <input type="text" class="form-control numeros" disabled value="<?= $datos_estudiante['nombre'] . ' ' . $datos_estudiante['apellido'] ?>">
                            </div>
                            <div class="col-lg-4 form-group">
                                <label>Genero</label>
                                <input type="text" class="form-control numeros" disabled value="<?= $genero ?>">
                            </div>
                            <div class="col-lg-4 form-group">
                                <label>Curso actual</label>
                                <input type="text" class="form-control numeros" disabled value="<?= $datos_estudiante['curso'] ?>">
                            </div>
                            <div class="col-lg-12 mt-4">
                                <h4 class="text-danger">Pagos realizados</h4>
                            </div>
                            <div class="table-responsive mt-4">
                                <table class="table table-hover table-borderless table-sm" width="100%" cellspacing="0">
                                    <thead>
                                        <tr class="text-center font-weight-bold">
                                            <th scope="col">Recibo de pago No.</th>
                                            <th scope="col">Mes pagado</th>
                                            <th scope="col">Mensualidad</th>
                                            <th scope="col">Descuento</th>
                                            <th scope="col">IVA</th>
                                            <th scope="col">Total pagado</th>
                                            <th scope="col">Plataforma</th>
                                            <th scope="col">Usuario realiza</th>
                                            <th scope="col">Fecha realizado</th>
                                        </tr>
                                    </thead>
                                    <tbody class="buscar">
                                        <?php
                                        foreach ($datos_pago as $pago) {
                                            $id_pago = $pago['id'];
                                            $valor = $pago['valor'];
                                            $iva = $pago['iva'];
                                            $descuento = $pago['descuento'];
                                            $total_pago = $pago['total_pago'];
                                            $plataforma = $pago['pago'];
                                            $usuario = $pago['usuario_realiza'];
                                            $fechareg = $pago['fechareg'];

                                            $meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
                                            $mes_letra = $meses[(date('n', strtotime($pago['anio_mes'])) * 1) - 1];
                                            $anio = date('Y', strtotime($pago['anio_mes']));
                                        ?>
                                            <tr class="text-center">
                                                <td><?= $id_pago ?></td>
                                                <td><?= $mes_letra . '-' . $anio ?></td>
                                                <td>$<?= number_format($valor) ?></td>
                                                <td>$<?= number_format($descuento) ?></td>
                                                <td>%<?= $iva ?></td>
                                                <td>$<?= number_format($total_pago) ?></td>
                                                <td><?= $plataforma ?></td>
                                                <td><?= $usuario ?></td>
                                                <td><?= $fechareg ?></td>
                                                <td>
                                                    <a href="<?= BASE_URL ?>imprimir/factura?pago=<?= base64_encode($id_pago) ?>" target="_blank" class="btn btn-primary btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Descargar factura">
                                                        <i class="fa fa-download"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    include_once VISTA_PATH . 'script_and_final.php';
}
