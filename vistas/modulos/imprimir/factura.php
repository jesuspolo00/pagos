<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre'] && $_SESSION['rol'] != 5) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'pagos' . DS . 'ControlPagos.php';

$instancia = ControlPagos::singleton_pagos();

if (isset($_GET['pago'])) {

    $id_pago = base64_decode($_GET['pago']);
    $datos_pago = $instancia->mostrarDatosPagoIdControl($id_pago);

    $meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
    $mes_letra = $meses[(date('n', strtotime($datos_pago['anio_mes'])) * 1) - 1];
    $anio = date('Y', strtotime($datos_pago['anio_mes']));

    $img = ($datos_pago['tipo_pago'] == 1) ? 'icaco.png' : 'mamiyo.png';
    $tamano = ($datos_pago['tipo_pago'] == 1) ? '92' : '50';
    $factura_nom = ($datos_pago['tipo_pago'] == 1) ? 'icaco' : 'mamiyo';

    class MYPDF extends TCPDF
    {

        public function setData($logo)
        {
            $this->logo = $logo;
        }

        public function Header()
        {
            /* $this->setJPEGQuality(90);
            $this->Image(PUBLIC_PATH . 'img/' . $this->logo, 0, 0, 210, 35);
            $this->Ln(30);
            $this->Cell(90);
            $this->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
            $this->Cell(12, 50, 'ENTREGA DE INVENTARIO', 0, 0, 'C'); */
        }

        public function Footer()
        {
            $this->SetY(-15);
            $this->SetFillColor(127);
            $this->SetTextColor(127);
            $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
            $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
        }
    }

    // create a PDF object
    $pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document (meta) information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->setData($datos_super_empresa['imagen']);
    $pdf->SetAuthor('Jesus Polo');
    $pdf->SetTitle('Factura');
    $pdf->SetSubject('Factura');
    $pdf->SetKeywords('Factura');
    $pdf->AddPage();

    for ($i = 0; $i < 2; $i++) {
        $pdf->Ln(0);
        $pdf->Cell(5);
        $html = '
        <table style="width:98%;" border="1">
        <tr style="text-align:center; font-size: 0.8em; font-weight: bold;">
        <td colspan="2" style="border:none;" rowspan="1"><img src="' . PUBLIC_PATH . 'img/' . $img . '" border="0" width="' . $tamano . '"></td>
        <td colspan="2" rowspan="1" style="border:none;">
        <br>
        <br>
        FACTURA DE PAGO
        </td>
        <td colspan="2" rowspan="1">
        <br>
        <br>
        cra 54 #96-43 -PBX. 3784156  B/QUILLA 
        <br>
        Programa PAGOS
        </td>
        </tr>
        </table>';

        // output the HTML content
        $pdf->writeHTMLCell(185, 0, '', '', $html, '', 1, 0, true, 'C', true);


        $pdf->Ln(5);
        $pdf->Cell(6);

        $tabla = '
	<table cellpadding="5" border="1" style="font-size:8.5px; width:100%; font-size: 0.8em; ">
	<tr style="text-align:center; font-weight:bold; text-transform: uppercase;">
        <th style="width: 18%;">Recibo de pago:</th>
        <th colspan="4">' . $datos_pago['id'] . '</th>
	</tr>
        <tr>
            <td>Cod. Niñ@:</td>
            <td colspan="4" style="text-align: center;">' . $datos_pago['codigo'] . '</td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td colspan="4" style="text-align: center;">' . $datos_pago['estudiante'] . '</td>
        </tr>
        <tr>
            <td>Curso:</td>
            <td colspan="4" style="text-align: center;">' . $datos_pago['curso'] . '</td>
        </tr>
        <tr>
            <td>Mes de Pago:</td>
            <td colspan="4" style="text-align: center;">' . $mes_letra . '-' . $anio . '</td>
        </tr>
        <tr>
            <td>Fecha:</td>
            <td colspan="4" style="text-align: center;">' . $datos_pago['fechareg'] . '</td>
        </tr>
        <tr>
            <td style="font-weight:bold; text-transform: uppercase;">Valor pagado:</td>
            <td colspan="4" style="text-align: center;">$' . number_format($datos_pago['total_pago']) . '</td>
        </tr>
	</table>
	';
        $pdf->writeHTML($tabla, true, false, true, false, '');

        $pdf->Cell(5);
        $pdf->Cell(182, 2, '-----------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $pdf->Ln(10);
    }

    $pdf->Output('factura_' . $factura_nom . '_' . date('Y-m-d-H-i-s') . '.pdf', 'I');
}
