<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'pagos' . DS . 'ModeloPagos.php';

class ControlPagos
{

    private static $instancia;

    public static function singleton_pagos()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }


    public function mostrarCursosControl()
    {
        $mostrar = ModeloPagos::mostrarCursosModel();
        return $mostrar;
    }

    public function mostrarEstudiantesControl()
    {
        $mostrar = ModeloPagos::mostrarEstudiantesModel();
        return $mostrar;
    }

    public function mostrarDatosEstudiantesControl($id)
    {
        $mostrar = ModeloPagos::mostrarDatosEstudiantesModel($id);
        return $mostrar;
    }

    public function mostrarEmpresaFacturaControl()
    {
        $mostrar = ModeloPagos::mostrarEmpresaFacturaModel();
        return $mostrar;
    }

    public function mostrarMesesControl()
    {
        $mostrar = ModeloPagos::mostrarMesesModel();
        return $mostrar;
    }

    public function mostrarAniosControl()
    {
        $mostrar = ModeloPagos::mostrarAniosModel();
        return $mostrar;
    }


    public function ultimoPagoEstudianteControl($id, $anio)
    {
        $mostrar = ModeloPagos::ultimoPagoEstudianteModel($id, $anio);
        return $mostrar;
    }


    public function mostrarDatosPagoIdControl($id)
    {
        $mostrar = ModeloPagos::mostrarDatosPagoIdModel($id);
        return $mostrar;
    }

    public function mostrarDatosPagoEstudianteControl($id)
    {
        $mostrar = ModeloPagos::mostrarDatosPagoEstudianteModel($id);
        return $mostrar;
    }

    public function guardarPagoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante']) &&
            isset($_POST['empresa_factura']) &&
            !empty($_POST['empresa_factura']) &&
            isset($_POST['mes']) &&
            !empty($_POST['mes'])
        ) {

            $descuento = str_replace(',', '', $_POST['descuento']);
            $iva = $_POST['iva'];
            $mensualidad = str_replace(',', '', $_POST['mensualidad']);

            $subtotal = $mensualidad - $descuento;
            if ($iva == 0) {
                $total = $subtotal;
            } else {
                $total = ($subtotal * $iva) / 100;
            }

            $anio_mes = $_POST['anio'] . '-' . $_POST['mes'];

            $datos = array(
                'anio_mes' => $anio_mes,
                'valor' => $mensualidad,
                'descuento' => $descuento,
                'iva' => $iva,
                'total_pago' => $total,
                'id_estudiante' => $_POST['id_estudiante'],
                'tipo_pago' => $_POST['empresa_factura'],
                'id_log' => $_POST['id_log']
            );

            $guardar = ModeloPagos::guardarPagoModel($datos);

            if ($guardar == TRUE) {
                echo '
                    <script>
                    ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("index");
                    }
                    </script>
                    ';
            } else {
                echo '
                    <script>
                    ohSnap("Error al guardar!", {color: "red", "duration": "1000"});
                    </script>
                    ';
            }
        }
    }
}
