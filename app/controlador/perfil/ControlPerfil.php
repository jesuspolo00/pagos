<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'perfil' . DS . 'ModeloPerfil.php';
require_once CONTROL_PATH . 'hash.php';

class ControlPerfil
{

	private static $instancia;

	public static function singleton_perfil()
	{
		if (!isset(self::$instancia)) {
			$miclase = __CLASS__;
			self::$instancia = new $miclase;
		}
		return self::$instancia;
	}

	public function mostrarDatosPerfilControl($id)
	{
		$datos = ModeloPerfil::mostrarDatosPerfilModel($id);
		return $datos;
	}

	public function guardarPerfilControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_log']) &&
			!empty($_POST['id_log']) &&
			isset($_POST['nombre']) &&
			!empty($_POST['nombre'])
		) {
			$pass_ol = $_POST['pass_old'];
			$pass = $_POST['password'];
			$conf_pass = $_POST['conf_password'];

			if ($pass == '' && $conf_pass == '') {
				$datos = array(
					'id_user' => $_POST['id_log'],
					'nombre' => $_POST['nombre'],
					'pass' => $pass_ol
				);
			}

			if ($conf_pass == $pass) {

				$pass_hash = Hash::hashpass($conf_pass);

				$datos = array(
					'id_user' => $_POST['id_log'],
					'nombre' => $_POST['nombre'],
					'pass' => $pass_hash
				);
			} else {
				echo '
                    <script>
                    ohSnap("Contraseñas no coinciden!", {color: "red", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("index");
                    }
                    </script>
                    ';
			}


			$guardar = ModeloPerfil::guardarPerfilModel($datos);

			if ($guardar == TRUE) {
				echo '
                    <script>
                    ohSnap("Actualizado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("index");
                    }
                    </script>
                    ';
			}
		}
	}
}
