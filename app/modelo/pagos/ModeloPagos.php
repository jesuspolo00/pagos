<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloPagos extends conexion
{

    public function mostrarCursosModel()
    {
        $tabla = 'cursos';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_categoria IN(SELECT c.id FROM curso_categoria c);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarEstudiantesModel()
    {
        $tabla = 'estudiantes';
        $cnx = conexion::singleton_conexion();

        $cmdsql = "SELECT e.*, 
        (SELECT (SELECT c.nombre FROM cursos c WHERE c.id = m.curso_id) AS curso FROM matriculas m WHERE m.estudiante_id = e.id ORDER BY id DESC LIMIT 1) AS curso
        FROM " . $tabla . " e;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public function mostrarDatosEstudiantesModel($id)
    {
        $tabla = 'estudiantes';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT *, (SELECT
        (SELECT c.nombre FROM cursos c WHERE c.id = curso_id) AS curso
        FROM matriculas WHERE estudiante_id = :id ORDER BY id DESC LIMIT 1) as curso FROM " . $tabla . " WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarEmpresaFacturaModel()
    {
        $tabla = 'tipo_pago';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla;
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarMesesModel()
    {
        $tabla = 'meses';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla;
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarAniosModel()
    {
        $tabla = 'anios';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla;
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function ultimoPagoEstudianteModel($id, $anio)
    {
        $tabla = 'pagos';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_estudiante = :id AND anio_mes = :a ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->bindValue(':a', $anio);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function guardarPagoModel($datos)
    {
        $tabla = 'pagos';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO pagos (anio_mes, valor, descuento, iva, total_pago, id_estudiante, tipo_pago, id_log) 
        VALUES (:an, :v, :d, :i, :t, :ide, :tp, :idl);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':an', $datos['anio_mes']);
            $preparado->bindParam(':v', $datos['valor']);
            $preparado->bindParam(':d', $datos['descuento']);
            $preparado->bindParam(':i', $datos['iva']);
            $preparado->bindParam(':t', $datos['total_pago']);
            $preparado->bindParam(':ide', $datos['id_estudiante']);
            $preparado->bindParam(':tp', $datos['tipo_pago']);
            $preparado->bindParam(':idl', $datos['id_log']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }




    public function mostrarDatosPagoIdModel($id)
    {
        $tabla = 'pagos';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT p.*,
        (SELECT e.documento FROM estudiantes e WHERE e.id = p.id_estudiante) AS codigo,
        (SELECT CONCAT(e.nombre, ' ', e.apellido) FROM estudiantes e WHERE e.id = p.id_estudiante) AS estudiante,
        (SELECT (SELECT c.nombre FROM cursos c WHERE c.id = m.curso_id) FROM matriculas m WHERE m.estudiante_id = p.id_estudiante ORDER BY m.id DESC LIMIT 1) AS curso
        FROM " . $tabla . " p WHERE p.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public function mostrarDatosPagoEstudianteModel($id)
    {
        $tabla = 'pagos';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT p.*,
        (SELECT t.nombre FROM tipo_pago t WHERE t.id = p.tipo_pago) AS pago,
        (SELECT u.usuario FROM usuarios u WHERE u.id = p.id_log) AS usuario_realiza
        FROM " . $tabla . " p WHERE p.id_estudiante = :id ORDER BY p.anio_mes ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}


