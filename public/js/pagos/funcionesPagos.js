$(document).ready(function() {


    $(".emp_fac").click(function() {
        var id = $(this).attr('id');
        var iva = $("#iva" + id).val();
        var men = $("#men" + id).val();
        var form = $('#pagar');


        $("#iva").val(iva);
        $(".mensualidad").val(number_format_js(men, 0, '.', ','));
        calcular(form);
    });


    $(".mensualidad").keyup(function() {
        var form = $('#pagar');
        calcular(form);
    });



    $(".descuento").keyup(function() {
        var form = $('#pagar');
        calcular(form);
    });


    function calcular(form) {
        $.ajax({
            type: "POST",
            url: '../vistas/ajax/pagos/calcular.php',
            data: form.serialize(),
            dataType: "JSON",
            success: function(r) {
                $(".total").val(number_format_js(r.total_final, 0, '.', ','));

                var descuento = (r.descuento == '') ? '' : number_format_js(r.descuento, 0, '.', ',');
                var mensualidad = (r.mensualidad == '') ? '' : number_format_js(r.mensualidad, 0, '.', ',');

                $(".descuento").val(descuento);
                $(".mensualidad").val(mensualidad);
            }
        });
    }



    function number_format_js(number, decimals, dec_point, thousands_point) {

        if (number == null || !isFinite(number)) {
            throw new TypeError("number is not valid");
        }

        if (!decimals) {
            var len = number.toString().split('.').length;
            decimals = len > 1 ? len : 0;
        }

        if (!dec_point) {
            dec_point = '.';
        }

        if (!thousands_point) {
            thousands_point = ',';
        }

        number = parseFloat(number).toFixed(decimals);

        number = number.replace(".", dec_point);

        var splitNum = number.split(dec_point);
        splitNum[0] = splitNum[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_point);
        number = splitNum.join(dec_point);

        return number;
    }

});